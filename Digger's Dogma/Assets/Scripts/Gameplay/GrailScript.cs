﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class GrailScript : MonoBehaviour
{
    const float TAKE_RADIUS = 0.8f;
    
    public GameObject player;
    

    void Update()
    {
        if (player != null)
        {
            Vector3 playerPos = player.transform.position;
            if ((playerPos - transform.position).magnitude <= TAKE_RADIUS)
            {
                Debug.Log("VICTORY");

                Destroy(gameObject);

                SceneManager.LoadScene(3);
            }
        }
    }
}
