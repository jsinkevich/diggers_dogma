﻿using UnityEngine;

public class HideIfPlayerNear : MonoBehaviour
{
    const float HIDE_RADIUS = 0.15f;

    [SerializeField] private GameObject _player;
    private SpriteRenderer _spriteRenderer;
    

    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
    

    void Update()
    {
        Vector3 playerPos = _player.transform.position;
        if ((playerPos - transform.position).magnitude <= HIDE_RADIUS)
        {
            _spriteRenderer.enabled = false;
        }
        else
        {
            _spriteRenderer.enabled = true;
        }
    }
}
