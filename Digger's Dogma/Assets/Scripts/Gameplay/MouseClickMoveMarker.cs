﻿using UnityEngine;

public class MouseClickMoveMarker : MonoBehaviour
{
    [SerializeField] private GameObject _markerSprite;
    private Animator _animator;
    public GameObject target;


    void Start()
    {
        _markerSprite = GameObject.Find("MarkerSprite");
        _animator = _markerSprite.GetComponent<Animator>();
    }


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            RaycastHit2D hitDirt = Physics2D.Raycast(mousePos, Vector2.zero, Mathf.Infinity, 1 << 15);
            RaycastHit2D hitRock = Physics2D.Raycast(mousePos, Vector2.zero, Mathf.Infinity, 1 << 16);

            if (hitDirt.collider != null)
            {
                Debug.Log(hitDirt.collider.gameObject.name);
                if (hitDirt.collider.gameObject.GetComponent<DirtScript>() != null)
                {
                    target = hitDirt.collider.gameObject;
                    _animator.SetInteger("AnimationNumber", 1);
                    transform.position = hitDirt.collider.gameObject.transform.position;
                }
                else
                {
                    target = null;
                    _animator.SetInteger("AnimationNumber", 0);
                    transform.position = mousePos;
                }
            }
            else if (hitRock.collider != null)
            {
                target = hitRock.collider.gameObject;
                _animator.SetInteger("AnimationNumber", 2);
                transform.position = hitRock.collider.gameObject.transform.position;
            }
            else
            {
                target = null;
                _animator.SetInteger("AnimationNumber", 0);
                transform.position = mousePos;
            }
        }
    }
}
