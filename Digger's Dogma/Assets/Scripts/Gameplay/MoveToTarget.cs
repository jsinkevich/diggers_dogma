﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToTarget : MonoBehaviour
{
    const float STOP_RADIUS = 0.1f;
    const float IMPACT_RADIUS = 2.5f;
    const float IMPACT_OFFSET_Y = -0.27f;

    const float PROGRESS_MAX = 100.0f;
    const float DIG_SPEED = 2.0f;
    const float MINE_SPEED = 0.75f;

    public float speed = 500;

    public GameObject marker;

    [SerializeField] private GameObject _sprite;
    [SerializeField] private GameObject _controllerUI;

    private PlayerController _controller;
    private Rigidbody2D _rigidbody2D;
    private Animator _animator;

    private Vector3 _movement = Vector3.zero;

    private float digProgress = 0;
    private float mineProgress = 0;

    
    void Start()
    {
        _controller = GetComponent<PlayerController>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = _sprite.GetComponent<Animator>();
    }

    public void ToStay()
    {
        _animator.SetInteger("AnimationNumber", 0);

        GameObject.Find("MarkerSprite").GetComponent<SpriteRenderer>().enabled = false;
        marker.transform.position = gameObject.transform.position;
    }


    void Update()
    {
        Vector2 targetPos = marker.transform.position;
        _movement = (targetPos - (Vector2)transform.position).normalized;
        _movement *= speed;
        _movement *= Time.deltaTime;

        if ((targetPos - (Vector2)transform.position).magnitude > STOP_RADIUS)
        {
            _movement = transform.TransformDirection(_movement);
        }
        else
        {
            _movement.Set(0, 0, 0);
        }

        _animator.SetFloat("Speed", _movement.magnitude);
        _rigidbody2D.velocity = _movement;


        GameObject target = marker.GetComponent<MouseClickMoveMarker>().target;
        if (target != null)
        {
            if (Vector2.Distance(transform.position - new Vector3(0, IMPACT_OFFSET_Y), target.transform.position) <= IMPACT_RADIUS)
            {
                if (target.gameObject.tag != "Diggable")
                {
                    digProgress = 0;
                }

                if (target.gameObject.tag == "Diggable")
                {
                    _animator.SetInteger("AnimationNumber", 1);
                    _animator.SetFloat("Speed", 0);

                    digProgress += DIG_SPEED;

                    if (digProgress >= PROGRESS_MAX)
                    {
                        digProgress = 0;
                        target.GetComponent<DirtScript>().Destroy();

                        ToStay();
                    }
                }
                else if (target.gameObject.tag == "Minable")
                {
                    if (_controller.pikeDurability > 0)
                    {
                        _animator.SetInteger("AnimationNumber", 2);
                        _animator.SetFloat("Speed", 0);

                        mineProgress += MINE_SPEED;

                        if (mineProgress >= PROGRESS_MAX)
                        {
                            mineProgress = 0;
                            target.GetComponent<ObstacleScript>().Destoy();
                            _controller.pikeDurability--;
                            _controllerUI.GetComponent<UIController>().UpdateCountPikes(_controller.pikeDurability);

                            ToStay();
                        }
                    }
                    else
                    {
                        
                    }
                }
            }
        }
    }
}
