﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleScript : MonoBehaviour
{
    [SerializeField] private GameObject _nextForm;

    public void Destoy()
    {
        if (_nextForm != null)
        {
            Instantiate(_nextForm, transform.position, transform.rotation);
        }
        Destroy(gameObject);
    }
}
