﻿using UnityEngine;

public class PikeScript : MonoBehaviour
{
    const float TAKE_RADIUS = 0.8f;

    public int count;

    public GameObject controllerUI;
    public GameObject player;
    
    void Update()
    {
        if (controllerUI != null && player != null)
        {
            Vector3 playerPos = player.transform.position;
            if ((playerPos - transform.position).magnitude <= TAKE_RADIUS)
            {
                PlayerController playerController = player.GetComponent<PlayerController>();

                playerController.pikeDurability += count;
                controllerUI.GetComponent<UIController>().UpdateCountPikes(playerController.pikeDurability);

                Destroy(gameObject);
            }
        }
    }
}
