﻿using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    const float CAMERA_SIZE_MIN = 6.0f;
    const float CAMERA_SIZE_MAX = 12.0f;

    public TextMeshProUGUI countPikesLabel;

    [SerializeField] private GameplaySaveLoader _gameplaySaveLoader;

    [SerializeField] private Canvas _gameplayCanvas;
    [SerializeField] private Canvas _victoryCanvas;
    [SerializeField] private Canvas _menuPauseCanvas;
    [SerializeField] private Canvas _loadGameCanvas;
    [SerializeField] private Canvas _saveGameCanvas;

    [SerializeField] private GameObject _loadGameContent;
    [SerializeField] private GameObject _saveGameContent;

    [SerializeField] private TMP_InputField _newSaveInputField;

    [SerializeField] private GameObject _prefabBlueButton;

    [SerializeField] private Camera _camera;

    [SerializeField] private Scrollbar _cameraSizeScroll;

    public Canvas GameplayCanvas { get => _gameplayCanvas; set => _gameplayCanvas = value; }

    public void UpdateCountPikes(int count)
    {
        countPikesLabel.text = count.ToString();
    }

    private void StopControl()
    {
        GameObject target = GameObject.Find("TargetMarker");
        GameObject player = GameObject.Find("Player");

        target.transform.position = player.transform.position;

        target.GetComponent<MouseClickMoveMarker>().enabled = false;
    }

    private void ReturnControl()
    {
        GameObject target = GameObject.Find("TargetMarker");
        target.GetComponent<MouseClickMoveMarker>().enabled = true;
    }

    public void OnOpenSettings()
    {
        StopControl();
        _gameplayCanvas.enabled = false;
        _menuPauseCanvas.enabled = true;
    }

    public void OnCloseSettings()
    {
        ReturnControl();
        _menuPauseCanvas.enabled = false;
        _gameplayCanvas.enabled = true;
    }

    public void OnCameraSizeChanged()
    {
        float size = CAMERA_SIZE_MIN + _cameraSizeScroll.value * (CAMERA_SIZE_MAX - CAMERA_SIZE_MIN);
        _camera.GetComponent<Camera>().orthographicSize = size;
    }

    public void OpenLoadGameMenu()
    {
        _menuPauseCanvas.enabled = false;
        _loadGameCanvas.enabled = true;

        int countChilds = _loadGameContent.transform.GetChildCount();
        for (int i = 0; i < countChilds; i++)
        {
            Destroy(_loadGameContent.transform.GetChild(i));
        }

        string loadDir = Directory.GetCurrentDirectory() + "/saves";
        string[] maps = Directory.GetFiles(loadDir, "*.json");
        int yPos = 0;
        foreach (var map in maps)
        {
            GameObject mapButton = Instantiate(_prefabBlueButton) as GameObject;
            mapButton.transform.SetParent(_loadGameContent.transform, false);
            RectTransform rect = mapButton.GetComponent<RectTransform>();
            rect.anchoredPosition = new Vector2(0, yPos);
            yPos -= 80;

            TextMeshProUGUI textObject = mapButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            string name = Path.GetFileName(map).Split('.')[0];
            textObject.text = Path.GetFileName(name);

            Button buttonComponent = mapButton.GetComponent<Button>();
            buttonComponent.onClick.AddListener(delegate { LoadGame(name); });
        }
    }

    public void OnLoadMenuClose()
    {
        _loadGameCanvas.enabled = false;
        _menuPauseCanvas.enabled = true;
    }

    public void OpenSaveGameMenu()
    {
        _menuPauseCanvas.enabled = false;
        _saveGameCanvas.enabled = true;

        int countChilds = _saveGameContent.transform.GetChildCount();
        for (int i = 0; i < countChilds; i++)
        {
            Destroy(_saveGameContent.transform.GetChild(i));
        }

        string loadDir = Directory.GetCurrentDirectory() + "/saves";
        string[] maps = Directory.GetFiles(loadDir, "*.json");
        int yPos = 0;
        foreach (var map in maps)
        {
            GameObject mapButton = Instantiate(_prefabBlueButton) as GameObject;
            mapButton.transform.SetParent(_saveGameContent.transform, false);
            RectTransform rect = mapButton.GetComponent<RectTransform>();
            rect.anchoredPosition = new Vector2(0, yPos);
            yPos -= 80;

            TextMeshProUGUI textObject = mapButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            string name = Path.GetFileName(map).Split('.')[0];
            textObject.text = Path.GetFileName(name);

            Button buttonComponent = mapButton.GetComponent<Button>();
            buttonComponent.onClick.AddListener(delegate { SaveGame(name); });
        }
    }

    public void OnNewSaveGame()
    {
        SaveGame(_newSaveInputField.text);
    }

    public void OnSaveMenuClose()
    {
        _saveGameCanvas.enabled = false;
        _menuPauseCanvas.enabled = true;
    }

    public void LoadGame(string saveName)
    {
        PlayerPrefs.SetInt("SavedGame", 1);
        PlayerPrefs.SetString("LoadMapName", saveName);

        SceneManager.LoadScene(1);
    }
    
    public void SaveGame(string saveName)
    {
        _gameplaySaveLoader.SaveScene(saveName);
    }

    public void Victory()
    {
        StopControl();
        _gameplayCanvas.enabled = false;
        _victoryCanvas.enabled = true;
    }

    public void Exit()
    {
        SceneManager.LoadScene(0);
    }
}
