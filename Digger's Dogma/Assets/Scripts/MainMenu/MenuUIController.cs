﻿using UnityEngine.SceneManagement;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using TMPro;

public class MenuUIController : MonoBehaviour
{

    [SerializeField] private GameObject _mainCanvas;
    [SerializeField] private GameObject _newOrLoadCanvas;
    [SerializeField] private GameObject _mapListCanvas;

    [SerializeField] private GameObject _mapsContent;
    [SerializeField] private GameObject _prefabBlueButton;

    private bool _playGame = true;

    public void OnNewGame()
    {
        _mainCanvas.SetActive(false);
        _newOrLoadCanvas.SetActive(true);
        _playGame = true;
    }

    public void OnMapEditor()
    {
        _mainCanvas.SetActive(false);
        _newOrLoadCanvas.SetActive(true);
        _playGame = false;
    }

    public void OnMapEditorMenuClose()
    {
        _newOrLoadCanvas.SetActive(false);
        _mainCanvas.SetActive(true);
        _playGame = true;
    }

    public void OnNewMap()
    {
        if (_playGame)
        {
            LoadMaps("maps", false);
        }
        else
        {
            PlayerPrefs.SetInt("MapLoad", 0);
            SceneManager.LoadScene(2);
        }
    }

    public void OnLoadMap()
    {
        if (_playGame)
        {
            LoadMaps("saves", true);
        }
        else
        {
            LoadMaps("maps", false);
        }
    }

    public void LoadMaps(string directory, bool savedGame)
    {
        int countChilds = _mapsContent.transform.GetChildCount();
        for (int i = 0; i < countChilds; i++)
        {
            Destroy(_mapsContent.transform.GetChild(i));
        }

        string mapDir = Directory.GetCurrentDirectory() + "/" + directory;
        string[] maps = Directory.GetFiles(mapDir, "*.json");
        int yPos = 0;
        foreach (var map in maps)
        {
            GameObject mapButton = Instantiate(_prefabBlueButton) as GameObject;
            mapButton.transform.SetParent(_mapsContent.transform, false);
            RectTransform rect = mapButton.GetComponent<RectTransform>();
            rect.anchoredPosition = new Vector2(0, yPos);
            yPos -= 80;
            
            TextMeshProUGUI textObject = mapButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            string name = Path.GetFileName(map).Split('.')[0];
            textObject.text = Path.GetFileName(name);

            Button buttonComponent = mapButton.GetComponent<Button>();
            if (_playGame)
            {
                if (savedGame)
                {
                    buttonComponent.onClick.AddListener(delegate { LoadSavedGame(name); });
                }
                else
                {
                    buttonComponent.onClick.AddListener(delegate { StartNewGame(name); });
                }
            }
            else
            {
                buttonComponent.onClick.AddListener(delegate { OpenMapToEdit(name); });
            }
        }
        
        _newOrLoadCanvas.SetActive(false);
        _mapListCanvas.SetActive(true);
    }

    public void OpenMapToEdit(string mapName)
    {
        PlayerPrefs.SetInt("MapLoad", 1);
        PlayerPrefs.SetString("LoadMapName", mapName);

        SceneManager.LoadScene(2);
    }

    public void StartNewGame(string mapName)
    {
        PlayerPrefs.SetInt("SavedGame", 0);
        PlayerPrefs.SetString("LoadMapName", mapName);

        SceneManager.LoadScene(1);
    }

    public void LoadSavedGame(string mapName)
    {
        PlayerPrefs.SetInt("SavedGame", 1);
        PlayerPrefs.SetString("LoadMapName", mapName);

        SceneManager.LoadScene(1);
    }

    public void MapListClose()
    {
        _mapListCanvas.SetActive(false);
        _mainCanvas.SetActive(true);
        _playGame = true;
    }

    public void Exit()
    {
        Application.Quit();
    }
}
