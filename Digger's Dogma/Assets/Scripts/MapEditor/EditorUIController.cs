﻿using Assets.Scripts.MapEditor;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EditorUIController : MonoBehaviour
{
    const float CAMERA_SIZE_MIN = 6.0f;
    const float CAMERA_SIZE_MAX = 18.0f;

    [SerializeField] private GameObject _camera;

    [SerializeField] private GameObject _mapBrush;
    private MapBrush _mapBrushComponent;

    [SerializeField] private GameObject _selectImage;

    [SerializeField] private GameObject _navigatorButton;
    [SerializeField] private GameObject _eraserButton;
    [SerializeField] private GameObject _startLocationButton;

    [SerializeField] private GameObject _wallButton;
    [SerializeField] private GameObject _monolithButton;

    [SerializeField] private GameObject _rockButton;
    [SerializeField] private GameObject _columnButton;
    [SerializeField] private GameObject _halfColumnButton;

    [SerializeField] private GameObject _pikeButton;
    [SerializeField] private GameObject _grailButton;

    [SerializeField] private Canvas _mainCanvas;
    [SerializeField] private Canvas _menuCanvas;

    [SerializeField] private Scrollbar _cameraSizeScroll;

    [SerializeField] private Toggle _showDirtToggle;
    [SerializeField] private Toggle _showObstaclesToggle;

    public void Start()
    {
        _mapBrushComponent = _mapBrush.GetComponent<MapBrush>();
    }

    public void OpenMenu()
    {
        _mainCanvas.enabled = false;
        _mapBrushComponent.toolsEnabled = false;
        _menuCanvas.enabled = true;
    }

    public void CloseMenu()
    {
        _mainCanvas.enabled = true;
        _mapBrushComponent.toolsEnabled = true;
        _menuCanvas.enabled = false;
    }

    public void OnNavigatorSelected()
    {
        _mapBrushComponent.selectedTool = Tools.Navigator;
        _selectImage.transform.position = _navigatorButton.transform.position;
    }

    public void OnEraserSelected()
    {
        _mapBrushComponent.selectedTool = Tools.Eraser;
        _selectImage.transform.position = _eraserButton.transform.position;
    }

    public void OnStartLocationSelected()
    {
        _mapBrushComponent.selectedTool = Tools.StartLocation;
        _selectImage.transform.position = _startLocationButton.transform.position;
    }

    public void OnWallSelected()
    {
        _mapBrushComponent.selectedTool = Tools.Wall;
        _selectImage.transform.position = _wallButton.transform.position;
    }

    public void OnMonolithSelected()
    {
        _mapBrushComponent.selectedTool = Tools.Monolith;
        _selectImage.transform.position = _monolithButton.transform.position;
    }

    public void OnRockSelected()
    {
        _mapBrushComponent.selectedTool = Tools.Rock;
        _selectImage.transform.position = _rockButton.transform.position;
    }

    public void OnColumnSelected()
    {
        _mapBrushComponent.selectedTool = Tools.Column;
        _selectImage.transform.position = _columnButton.transform.position;
    }

    public void OnHalfColumnSelected()
    {
        _mapBrushComponent.selectedTool = Tools.HalfColumn;
        _selectImage.transform.position = _halfColumnButton.transform.position;
    }

    public void OnPikeSelected()
    {
        _mapBrushComponent.selectedTool = Tools.Pike;
        _selectImage.transform.position = _pikeButton.transform.position;
    }

    public void OnGrailSelected()
    {
        _mapBrushComponent.selectedTool = Tools.Grail;
        _selectImage.transform.position = _grailButton.transform.position;
    }

    public void OnCameraSizeChanged()
    {
        float size = CAMERA_SIZE_MIN + _cameraSizeScroll.value * (CAMERA_SIZE_MAX - CAMERA_SIZE_MIN);
        _camera.GetComponent<Camera>().orthographicSize = size;
    }

    public void OnCheckShowDirt()
    {
        GameObject[] dirtBoxes = GameObject.FindGameObjectsWithTag("Diggable");
        foreach (GameObject dirtBox in dirtBoxes)
        {
            dirtBox.GetComponent<SpriteRenderer>().enabled = _showDirtToggle.isOn;
        }
    }

    public void OnCheckShowObstacles()
    {
        GameObject[] obstacles = GameObject.FindGameObjectsWithTag("Minable");
        foreach (GameObject obstacle in obstacles)
        {
            obstacle.GetComponent<SpriteRenderer>().enabled = _showObstaclesToggle.isOn;
        }
    }

    public void Exit()
    {
        SceneManager.LoadScene(0);
    }
}
