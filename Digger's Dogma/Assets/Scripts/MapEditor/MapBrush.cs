﻿using Assets.Scripts.MapEditor;
using UnityEngine;

public class MapBrush : MonoBehaviour
{
    const float CELL_SIZE = 2f;

    const int LEFT_PANEL_WIDTH = 256;
    const int HEAD_HEIGHT = 48;

    public Tools selectedTool;

    [SerializeField] private GameObject _camera;

    [SerializeField] private GameObject _startLocation;
    [SerializeField] private GameObject _prefabWall;
    [SerializeField] private GameObject _prefabMonolith;
    [SerializeField] private GameObject _prefabRock;
    [SerializeField] private GameObject _prefabColumn;
    [SerializeField] private GameObject _prefabHalfColumn;
    [SerializeField] private GameObject _prefabPike;
    [SerializeField] private GameObject _prefabGrail;

    public bool toolsEnabled = true;

    private float _navigationOffsetX;
    private float _navigationOffsetY;


    public void Start()
    {
        SetNavigationOffset();
    }

    public void SetNavigationOffset()
    {
        float halfScreenWidth = Screen.width / 2;
        float halfScreenHeight = Screen.height / 2;

        Vector3 baseCenter = Camera.main.ScreenToWorldPoint(new Vector3(halfScreenWidth, halfScreenHeight, 0));
        Vector3 newCenter = Camera.main.ScreenToWorldPoint(new Vector3(
                halfScreenWidth + LEFT_PANEL_WIDTH / 2,
                halfScreenHeight - (HEAD_HEIGHT / 2), 0));
        Vector3 offsetVector = newCenter - baseCenter;

        _navigationOffsetX = offsetVector.x;
        _navigationOffsetY = offsetVector.y;
    }

    private Vector2 ToCellPosition(Vector2 position)
    {
        return new Vector2(Mathf.Round(position.x / CELL_SIZE) * CELL_SIZE,
            Mathf.Round(position.y / CELL_SIZE) * CELL_SIZE);
    }

    private bool IsInScene(Vector3 mousePos)
    {
        return mousePos.x > LEFT_PANEL_WIDTH &&
                mousePos.y < Screen.height - HEAD_HEIGHT;
    }
    
    public void Update()
    {
        if (Input.GetMouseButtonDown(0) && IsInScene(Input.mousePosition) && toolsEnabled)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            Vector2 cellPos = ToCellPosition(mousePos);

            RaycastHit2D hitDirt = Physics2D.Raycast(mousePos, Vector2.zero, Mathf.Infinity, 1 << 15);
            RaycastHit2D hitObstacle = Physics2D.Raycast(mousePos, Vector2.zero, Mathf.Infinity, 1 << 16);
            RaycastHit2D hitItem = Physics2D.Raycast(mousePos, Vector2.zero, Mathf.Infinity, 1 << 17);

            if (hitItem.collider != null)
            {
                Debug.Log(hitItem.collider.gameObject.name);
            }

            if (selectedTool == Tools.Navigator)
            {
                _camera.transform.position = new Vector3(mousePos.x - _navigationOffsetX, mousePos.y - _navigationOffsetY, -10);
            }
            else if (selectedTool == Tools.Eraser)
            {
                if (hitDirt.collider != null)
                {
                    Destroy(hitDirt.collider.gameObject);
                }
                else if (hitObstacle.collider != null)
                {
                    Destroy(hitObstacle.collider.gameObject);
                }
                else if (hitItem.collider != null)
                {
                    Destroy(hitItem.collider.gameObject);
                }
            }
            else if (selectedTool == Tools.StartLocation)
            {
                _startLocation.transform.position = cellPos;
            }

            else if (selectedTool == Tools.Wall)
            {
                if (hitDirt.collider != null)
                {
                    Destroy(hitDirt.collider.gameObject);
                }
                Instantiate(_prefabWall, cellPos, transform.rotation);
            }
            else if (selectedTool == Tools.Monolith)
            {
                if (hitDirt.collider != null)
                {
                    Destroy(hitDirt.collider.gameObject);
                }
                Instantiate(_prefabMonolith, cellPos, transform.rotation);
            }

            else if (selectedTool == Tools.Rock)
            {
                if (hitObstacle.collider != null)
                {
                    Destroy(hitObstacle.collider.gameObject);
                }
                Instantiate(_prefabRock, cellPos, transform.rotation);
            }
            else if (selectedTool == Tools.Column)
            {
                if (hitObstacle.collider != null)
                {
                    Destroy(hitObstacle.collider.gameObject);
                }
                Instantiate(_prefabColumn, cellPos, transform.rotation);
            }
            else if (selectedTool == Tools.HalfColumn)
            {
                if (hitObstacle.collider != null)
                {
                    Destroy(hitObstacle.collider.gameObject);
                }
                Instantiate(_prefabHalfColumn, cellPos, transform.rotation);
            }

            else if (selectedTool == Tools.Pike)
            {
                if (hitItem.collider != null)
                {
                    Destroy(hitItem.collider.gameObject);
                }
                Instantiate(_prefabPike, cellPos, transform.rotation);
            }
            else if (selectedTool == Tools.Grail)
            {
                if (hitItem.collider != null)
                {
                    Destroy(hitItem.collider.gameObject);
                }
                Instantiate(_prefabGrail, cellPos, transform.rotation);
            }
        }
    }
}
