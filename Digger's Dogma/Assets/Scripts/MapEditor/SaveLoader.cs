﻿using Assets.Scripts.Model;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using UnityEngine;
using TMPro;

public class SaveLoader : MonoBehaviour
{
    [SerializeField] private GameObject _startLocation;
    [SerializeField] private TMP_InputField _mapNameInput;
    
    [SerializeField] private GameObject _prefabDirtBox;
    [SerializeField] private GameObject _prefabMonolith;
    [SerializeField] private GameObject _prefabRock;
    [SerializeField] private GameObject _prefabColumn;
    [SerializeField] private GameObject _prefabHalfColumn;
    [SerializeField] private GameObject _prefabPike;
    [SerializeField] private GameObject _prefabGrail;

    private string mapDir = Directory.GetCurrentDirectory() + "/maps/";

    public MapModel GenerateMapModel()
    {
        MapModel mapModel = new MapModel();
        
        mapModel.Player.CountPikes = 0;
        mapModel.Player.Position = _startLocation.transform.position;

        GameObject[] dirtBoxes = GameObject.FindGameObjectsWithTag("Diggable");
        List<Vector2> dirtBoxPositions = new List<Vector2>();
        foreach (GameObject dirtBox in dirtBoxes)
        {
            dirtBoxPositions.Add(dirtBox.transform.position);
        }
        mapModel.AllMapObjects.Add("DirtBoxes", dirtBoxPositions);

        GameObject[] monolithes = GameObject.FindGameObjectsWithTag("Monolith");
        List<Vector2> monolithPositions = new List<Vector2>();
        foreach (GameObject monolith in monolithes)
        {
            monolithPositions.Add(monolith.transform.position);
        }
        mapModel.AllMapObjects.Add("Monolithes", monolithPositions);

        GameObject[] minables = GameObject.FindGameObjectsWithTag("Minable");
        List<Vector2> rockPositions = new List<Vector2>();
        List<Vector2> columnPositions = new List<Vector2>();
        List<Vector2> halfColumnPositions = new List<Vector2>();
        foreach (GameObject minable in minables)
        {
            if (minable.name.Contains("HalfColumn"))
            {
                halfColumnPositions.Add(minable.transform.position);
            }
            else if (minable.name.Contains("Column"))
            {
                columnPositions.Add(minable.transform.position);
            }
            else if (minable.name.Contains("Rock"))
            {
                rockPositions.Add(minable.transform.position);
            }
        }
        mapModel.AllMapObjects.Add("HalfColumns", halfColumnPositions);
        mapModel.AllMapObjects.Add("Columns", columnPositions);
        mapModel.AllMapObjects.Add("Rocks", rockPositions);

        GameObject[] pikes = GameObject.FindGameObjectsWithTag("Pike");
        List<Vector2> pikePositions = new List<Vector2>();
        foreach (GameObject pike in pikes)
        {
            pikePositions.Add(pike.transform.position);
        }
        mapModel.AllMapObjects.Add("Pikes", pikePositions);

        GameObject[] grails = GameObject.FindGameObjectsWithTag("Grail");
        List<Vector2> grailPositions = new List<Vector2>();
        foreach (GameObject grail in grails)
        {
            grailPositions.Add(grail.transform.position);
        }
        mapModel.AllMapObjects.Add("Grails", grailPositions);

        return mapModel;
    }

    public void InstantiateMapModel(MapModel mapModel)
    {
        _startLocation.transform.position = mapModel.Player.Position;

        List<Vector2> dirtBoxPositions = mapModel.AllMapObjects["DirtBoxes"];
        foreach (Vector2 position in dirtBoxPositions)
        {
            Instantiate(_prefabDirtBox, position, transform.rotation);
        }

        List<Vector2> monolithPositions = mapModel.AllMapObjects["Monolithes"];
        foreach (Vector2 position in monolithPositions)
        {
            Instantiate(_prefabMonolith, position, transform.rotation);
        }

        List<Vector2> rockPositions = mapModel.AllMapObjects["Rocks"];
        foreach (Vector2 position in rockPositions)
        {
            Instantiate(_prefabRock, position, transform.rotation);
        }

        List<Vector2> halfColumnPositions = mapModel.AllMapObjects["HalfColumns"];
        foreach (Vector2 position in halfColumnPositions)
        {
            Instantiate(_prefabHalfColumn, position, transform.rotation);
        }

        List<Vector2> columnPositions = mapModel.AllMapObjects["Columns"];
        foreach (Vector2 position in columnPositions)
        {
            Instantiate(_prefabColumn, position, transform.rotation);
        }

        List<Vector2> pikePositions = mapModel.AllMapObjects["Pikes"];
        foreach (Vector2 position in pikePositions)
        {
            Instantiate(_prefabPike, position, transform.rotation);
        }

        List<Vector2> grailPositions = mapModel.AllMapObjects["Grails"];
        foreach (Vector2 position in grailPositions)
        {
            Instantiate(_prefabGrail, position, transform.rotation);
        }
    }

    void Start()
    {
        if (PlayerPrefs.GetInt("MapLoad") != 0)
        {
            string mapName = PlayerPrefs.GetString("LoadMapName");
            _mapNameInput.text = mapName;

            LoadScene(mapName);
        }
    }

    public void SaveScene()
    {
        MapModel mapModel = GenerateMapModel();

        DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(MapModel));
        using (FileStream fs = new FileStream(mapDir + _mapNameInput.text + ".json", FileMode.OpenOrCreate))
        {
            jsonFormatter.WriteObject(fs, mapModel);
        }
    }

    public void LoadScene(string filename)
    {
        DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(MapModel));
        using (FileStream fs = new FileStream(mapDir + filename + ".json", FileMode.Open))
        {
            MapModel mapModel = (MapModel)jsonFormatter.ReadObject(fs);

            InstantiateMapModel(mapModel);
        }
    }
}
