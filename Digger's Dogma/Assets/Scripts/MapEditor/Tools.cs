﻿namespace Assets.Scripts.MapEditor
{
    public enum Tools
    {
        Navigator,
        Eraser,
        StartLocation,

        Wall,
        Monolith,

        Rock,
        Column,
        HalfColumn,

        Pike,
        Grail
    }
}
