﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

namespace Assets.Scripts.Model
{
    [DataContract]
    public class MapModel
    {
        [DataMember]
        public Player Player { get; set; }

        [DataMember]
        public Dictionary<string, List<Vector2>> AllMapObjects { get; set; }


        public MapModel()
        {
            Player = new Player();
            AllMapObjects = new Dictionary<string, List<Vector2>>();
        }
    }
}
