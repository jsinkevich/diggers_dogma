﻿using System.Runtime.Serialization;
using UnityEngine;

namespace Assets.Scripts.Model
{
    [DataContract]
    public class Player
    {
        [DataMember]
        public int CountPikes { get; set; }

        [DataMember]
        public Vector2 Position { get; set; }
    }
}
