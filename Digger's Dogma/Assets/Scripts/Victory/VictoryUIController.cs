﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class VictoryUIController : MonoBehaviour
{
    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
